import { IInterview, InterviewModel } from "./interview-model";

export class InterviewModule {
    private static interviewModel: InterviewModel;

    public static init() {
        InterviewModule.interviewModel = new InterviewModel();
    }

    public static async saveInterview(interview: IInterview): Promise<void> {
        await InterviewModule.interviewModel.saveInterview(interview);
    }

    public static async getInterviews(): Promise<IInterview[]> {
        return await InterviewModule.interviewModel.getInterviews();
    }
}