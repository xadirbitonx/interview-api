import {Model, Schema} from "mongoose";
import * as mongoose from "mongoose";

export class InterviewModel {
    private static model: Model<IInterview>;

    public constructor() {
        InterviewModel.model = mongoose.model<IInterview>('interviews', interviewSchema);
    }

    public async saveInterview(interview: IInterview): Promise<void> {
        const applicantInterview = await InterviewModel.model.findOne({TZ: interview.TZ, interviewDate: interview.interviewDate});

        if (!applicantInterview) {
            await InterviewModel.model.create(interview);
        }
    }

    public async getInterviews(): Promise<IInterview[]> {
        return InterviewModel.model.find().exec();
    }
}

export interface IInterview {
    name: '',
    TZ: number,
    interviewDate: Date,
    profile: number,
    address: '',
    age: number,
    imagePath: '',
    image: '',
    interviewers: string[],
    preInterview: '',
    personalBackground: '',
    highschoolName: '',
    megama: '',
    highschoolNotes: '',
    highschoolNotes2: '',
    goodTraits: '',
    badTraits: '',
    creativity: '',
    motivation: '',
    difficulties: '',
    successExample: '',
    failureExample: '',
    project: '',
    theoryQuestion1: '',
    theoryQuestion2: '',
    theoryAnswer1: '',
    theoryAnswer2: '',
    theoryScore1: number,
    theoryScore2: number,
    practicalQuestion1: '',
    practicalQuestion2: '',
    practicalAnswer1: '',
    practicalAnswer2: '',
    practicalScore1: number,
    practicalScore2: number,
    logicalQuestionLevel1: number,
    logicalQuestionLevel2: number,
    logicalQuestion1: '',
    logicalQuestion2: '',
    logicalAnswer1: '',
    logicalAnswer2: '',
    logicalScore1: number,
    logicalScore2: number,
    oralExpression: '',
    freeText: '',
    theoryOverallScore: '',
    practicalOverallScore: '',
    technologicalOrientationScore: '',
    professionalPotentialScore: '',
    criticalThinkingScore: '',
    overallProfessionalScore: '',
    peopleSkillsScore: '',
    characteristicsScore: '',
    motivationScore: '',
    overallCharacterScore: '',
    overAllScore: ''
}

const interviewSchema = new Schema<IInterview>({
    name: {type: String, required: false},
    TZ: {type: Number, required: false, index: true},
    interviewDate: {type: Date, required: false},
    profile: {type: Number, required: false},
    address: {type: String, required: false},
    age: {type: Number, required: false},
    imagePath: {type: String, required: false},
    image: {type: String, required: false},
    interviewers: {type: [String], required: false},
    preInterview: {type: String, required: false},
    personalBackground: {type: String, required: false},
    highschoolName: {type: String, required: false},
    megama: {type: String, required: false},
    highschoolNotes: {type: String, required: false},
    highschoolNotes2: {type: String, required: false},
    goodTraits: {type: String, required: false},
    badTraits: {type: String, required: false},
    creativity: {type: String, required: false},
    motivation: {type: String, required: false},
    difficulties: {type: String, required: false},
    successExample: {type: String, required: false},
    failureExample: {type: String, required: false},
    project: {type: String, required: false},
    theoryQuestion1: {type: String, required: false},
    theoryQuestion2: {type: String, required: false},
    theoryAnswer1: {type: String, required: false},
    theoryAnswer2: {type: String, required: false},
    theoryScore1: {type: Number, required: false},
    theoryScore2: {type: Number, required: false},
    practicalQuestion1: {type: String, required: false},
    practicalQuestion2: {type: String, required: false},
    practicalAnswer1: {type: String, required: false},
    practicalAnswer2: {type: String, required: false},
    practicalScore1: {type: Number, required: false},
    practicalScore2: {type: Number, required: false},
    logicalQuestionLevel1: {type: Number, required: false},
    logicalQuestionLevel2: {type: Number, required: false},
    logicalQuestion1: {type: String, required: false},
    logicalQuestion2: {type: String, required: false},
    logicalAnswer1: {type: String, required: false},
    logicalAnswer2: {type: String, required: false},
    logicalScore1: {type: Number, required: false},
    logicalScore2: {type: Number, required: false},
    oralExpression: {type: String, required: false},
    freeText: {type: String, required: false},
    theoryOverallScore: {type: String, required: false},
    practicalOverallScore: {type: String, required: false},
    technologicalOrientationScore: {type: String, required: false},
    professionalPotentialScore: {type: String, required: false},
    criticalThinkingScore: {type: String, required: false},
    overallProfessionalScore: {type: String, required: false},
    peopleSkillsScore: {type: String, required: false},
    characteristicsScore: {type: String, required: false},
    motivationScore: {type: String, required: false},
    overallCharacterScore: {type: String, required: false},
    overAllScore: {type: String, required: false}
}, {
    minimize: false
});