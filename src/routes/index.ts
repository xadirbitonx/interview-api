import PromiseRouter from "express-promise-router";
import {InterviewModule} from "../modules/interview/interview-module";
import multer  from "multer";
import { IInterview } from "../modules/interview/interview-model";

export function createIndexRoute() {
    const indexRoute = PromiseRouter();
    const upload = multer();

    indexRoute.get('/isAlive', function (req, res, next) {
        res.status(200).json("wowowo");
    });

    indexRoute.get('/getInterviews', async function (req, res, next) {
        const interviews = await InterviewModule.getInterviews();
        setTimeout(() => {
            res.status(200).json(interviews);
        }, 500)
    });

    indexRoute.post('/saveInterview', upload.single('file'), async function (req, res, next) {
        const file = req.file;

        if (file) {
            const interview: IInterview = JSON.parse(file.buffer.toString())
            await InterviewModule.saveInterview(interview);
            res.status(200).send();
        } else {
            throw new Error("no file");
        }
    });

    return indexRoute;
}
